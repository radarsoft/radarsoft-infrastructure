terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.41"
    }

    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "~> 3.3.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "~> 2.2.2"
    }

    null = {
      source  = "hashicorp/null"
      version = "~> 3.1.1"
    }

		remote = {
			source = "tenstad/remote"
			version = "~> 0.1.2"
		}
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

provider "cloudflare" {
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
}

data "hcloud_image" "debian-arm" {
  name              = "debian-12"
  with_architecture = "arm"
}

data "hcloud_image" "debian" {
  name              = "debian-12"
  with_architecture = "x86"
}

resource "tls_private_key" "ssh-key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_sensitive_file" "private_key" {
  content         = tls_private_key.ssh-key.private_key_openssh
  filename        = "${path.module}/infrastructure_rsa"
  file_permission = "0400"
}

resource "hcloud_ssh_key" "infrastructure" {
  name       = "infrastructure-key"
  public_key = tls_private_key.ssh-key.public_key_openssh
}

resource "hcloud_server" "main" {
  name        = "radarsoft"
  server_type = "cax21"
  image       = data.hcloud_image.debian-arm.id
  location    = "fsn1"
  ssh_keys    = ["${hcloud_ssh_key.infrastructure.id}"]
  keep_disk   = true
}

resource "hcloud_volume" "data_volume" {
  name              = "data"
  size              = 100
  location          = "fsn1"
  format            = "ext4"
  delete_protection = true
}

resource "hcloud_volume_attachment" "data_attachment" {
  volume_id = hcloud_volume.data_volume.id
  server_id = hcloud_server.main.id
  automount = false
}

resource "null_resource" "mount_volume" {
  triggers = {
    hetzner_server_id = hcloud_volume_attachment.data_attachment.id
  }

  connection {
    private_key = tls_private_key.ssh-key.private_key_openssh
    host        = hcloud_server.main.ipv4_address
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir /mnt/data",
      "mount -o discard,defaults ${hcloud_volume.data_volume.linux_device} /mnt/data",
      "echo '${hcloud_volume.data_volume.linux_device} /mnt/data ext4 discard,nofail,defaults 0 0' >> /etc/fstab"
    ]
  }
}

resource "null_resource" "k3s-install-control" {
  triggers = {
    hetzner_server_id = null_resource.mount_volume.id
  }

  connection {
    private_key = tls_private_key.ssh-key.private_key_openssh
    host        = hcloud_server.main.ipv4_address
  }

  provisioner "remote-exec" {
    inline = ["curl -sfL https://get.k3s.io | INSTALL_K3S_CHANNEL=v1.27.3+k3s1 K3S_EXTERNAL_IP=${hcloud_server.main.ipv4_address} sh -"]
  }
}

resource "null_resource" "k3s-disable-control" {
  triggers = {
    install_id = null_resource.k3s-install-control.id
  }

  connection {
    private_key = tls_private_key.ssh-key.private_key_openssh
    host        = hcloud_server.main.ipv4_address
  }

  provisioner "file" {
    content     = "disable: ['servicelb', 'traefik']\n"
    destination = "/etc/rancher/k3s/config.yaml"
  }
}

resource "null_resource" "k3s-restart-control" {
  triggers = {
    disable_id = null_resource.k3s-disable-control.id
  }

  connection {
    private_key = tls_private_key.ssh-key.private_key_openssh
    host        = hcloud_server.main.ipv4_address
  }

  provisioner "remote-exec" {
    inline = ["systemctl restart k3s"]
  }
}

resource "null_resource" "k3s-local-get-remote-conf-control" {
  triggers = {
    restart_id = null_resource.k3s-restart-control.id
  }

  provisioner "local-exec" {
    command = "ssh-keygen -f ~/.ssh/known_hosts -R '${hcloud_server.main.ipv4_address}' && scp -i infrastructure_rsa -o StrictHostKeyChecking=no root@${hcloud_server.main.ipv4_address}:/etc/rancher/k3s/k3s.yaml tmp/radarsoft-infra-temp.yml"
  }
}

resource "null_resource" "k3s-local-create-config" {
  triggers = {
    get_remote_conf_id = null_resource.k3s-local-get-remote-conf-control.id
  }

  provisioner "local-exec" {
    command = "echo '${"${templatefile("templates/config.tftpl", {
      ca_data          = "${yamldecode(file("tmp/radarsoft-infra-temp.yml"))["clusters"][0]["cluster"]["certificate-authority-data"]}",
      remote_address   = "${hcloud_server.main.ipv4_address}",
      client_cert_data = "${yamldecode(file("tmp/radarsoft-infra-temp.yml"))["users"][0]["user"]["client-certificate-data"]}",
      client_key_data  = "${yamldecode(file("tmp/radarsoft-infra-temp.yml"))["users"][0]["user"]["client-key-data"]}"
    })}"}' > /Users/radar/.kube/config"
  }
}

resource "null_resource" "code-server-create-settings" {
  triggers = {
    get_remote_conf_id = null_resource.k3s-local-create-config.id
  }

  connection {
    private_key = tls_private_key.ssh-key.private_key_openssh
    host        = hcloud_server.main.ipv4_address
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /mnt/data/code-server/.vscode /mnt/data/code-server/dev",
      "touch /mnt/data/code-server/.vscode/settings.json",
      "chown 1000:1000 -R /mnt/data/code-server/dev"
    ]
  }
}

resource "null_resource" "create-metallb" {
  triggers = {
    hetzner_server_id = hcloud_server.main.id
  }

  provisioner "local-exec" {
    command = "sed 's/__REMOTE_ADDRESS__/${hcloud_server.main.ipv4_address}/g' templates/metallb.tftpl > kubernetes/system/metallb.yml"
  }
}

resource "null_resource" "apparmor" {
  triggers = {
    metallb_id = null_resource.create-metallb.id
  }

  connection {
    private_key = tls_private_key.ssh-key.private_key_openssh
    host        = hcloud_server.main.ipv4_address
  }

  provisioner "remote-exec" {
    inline = [
      "apt update",
      "apt install -y apparmor apparmor-utils"
    ]
  }
}

resource "cloudflare_record" "base" {
  zone_id = var.radarosft_zone_id
  name    = "radarsoft.cz"
  value   = hcloud_server.main.ipv4_address
  type    = "A"
  proxied = true
}

resource "cloudflare_record" "ssh-server" {
  zone_id = var.radarosft_zone_id
  name    = "ssh"
  value   = hcloud_server.main.ipv4_address
  type    = "A"
  proxied = false
}

resource "cloudflare_record" "code-server" {
  zone_id = var.radarosft_zone_id
  name    = "code"
  value   = "radarsoft.cz"
  type    = "CNAME"
  proxied = true
}

resource "cloudflare_record" "monitoring" {
  zone_id = var.radarosft_zone_id
  name    = "monitoring"
  value   = "radarsoft.cz"
  type    = "CNAME"
  proxied = true
}

resource "cloudflare_record" "grafana" {
  zone_id = var.radarosft_zone_id
  name    = "grafana"
  value   = "radarsoft.cz"
  type    = "CNAME"
  proxied = true
}

resource "cloudflare_record" "hasura" {
  zone_id = var.radarosft_zone_id
  name    = "hasura"
  value   = "radarsoft.cz"
  type    = "CNAME"
  proxied = true
}

resource "cloudflare_record" "tgbot" {
  zone_id = var.radarosft_zone_id
  name    = "tgbot"
  value   = "radarsoft.cz"
  type    = "CNAME"
  proxied = true
}

resource "cloudflare_record" "api" {
  zone_id = var.radarosft_zone_id
  name    = "api"
  value   = "radarsoft.cz"
  type    = "CNAME"
  proxied = true
}

resource "cloudflare_record" "dbleki" {
  zone_id = var.dbleki_zone_id
  name    = "dbleki.art"
  value   = hcloud_server.main.ipv4_address
  type    = "A"
  proxied = true
}

provider "remote" {
	alias = "main"
	max_sessions = 1

	conn {
		host = "${hcloud_server.main.ipv4_address}"
		user = "root"
		private_key = tls_private_key.ssh-key.private_key_openssh
		sudo = true
	}
}

data "remote_file" "control-plane-token-file" {
	depends_on = [hcloud_server.main]
	provider = remote.main
	path = "/var/lib/rancher/k3s/server/node-token"
}
