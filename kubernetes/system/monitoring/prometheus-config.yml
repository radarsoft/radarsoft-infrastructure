---

apiVersion: v1
kind: ConfigMap
metadata:
  namespace: monitoring
  name: prometheus-env
data:
  url: 'http://monitoring.radarsoft.cz/prometheus/'
  storage-retention: '168h'
  storage-min-block: '1h'
  storage-max-block: '4h'

---

apiVersion: v1
kind: ConfigMap
metadata:
  namespace: monitoring
  name: prometheus
data:
  alerts.yaml: |-
    groups:
    - name: default alert group
      rules: []

  recorded-rules.yaml: |-
    groups:
      - name: pods-metrics
        rules:
          - record: pod_cpu_usage
            expr: sum(rate(container_cpu_usage_seconds_total{image!="", pod!=""}[1m])) by (pod, namespace, kubernetes_io_hostname)
          - record: pod_memory_usage
            expr: sum(container_memory_usage_bytes{image!="", pod!=""}) by (pod, namespace, kubernetes_io_hostname)
          - record: pod_container_cpu_usage
            expr: sum(rate(container_cpu_usage_seconds_total{image!="", pod!=""}[1m])) by (pod, container_name, namespace, kubernetes_io_hostname)
          - record: pod_container_memory_usage
            expr: sum(container_memory_usage_bytes{image!="", pod!=""}) by (pod, container_name, namespace, kubernetes_io_hostname)

          - record: kube_pod_container_resource_limits_cpu_cores
            expr: max(kube_pod_container_resource_limits{unit="core"}) by (pod, namespace)
          - record: kube_pod_container_resource_requests_cpu_cores
            expr: max(kube_pod_container_resource_requests{unit="core"}) by (pod, namespace)

          - record: pod_cpu_limit
            expr: max(kube_pod_container_resource_limits_cpu_cores) by (pod, namespace)
          - record: pod_cpu_request
            expr: max(kube_pod_container_resource_requests_cpu_cores) by (pod, namespace)

          - record: running_pod_cpu_limit
            expr: kube_pod_container_resource_limits_cpu_cores * on (pod, namespace) group_left max(kube_pod_container_status_running) by (pod, namespace) > 0
          - record: running_pod_cpu_request
            expr: kube_pod_container_resource_requests_cpu_cores * on (pod, namespace) group_left max(kube_pod_container_status_running) by (pod, namespace) > 0
          - record: running_pod_cpu_usage
            expr: pod_cpu_usage * on (pod, namespace) group_left max(kube_pod_container_status_running) by (pod, namespace) > 0

          - record: kube_pod_container_resource_limits_memory_bytes
            expr: max(kube_pod_container_resource_limits{unit="byte"}) by (pod, namespace)
          - record: kube_pod_container_resource_requests_memory_bytes
            expr: max(kube_pod_container_resource_requests{unit="byte"}) by (pod, namespace)

          - record: pod_memory_limit
            expr: max(kube_pod_container_resource_limits_memory_bytes) by (pod, namespace)
          - record: pod_memory_request
            expr: max(kube_pod_container_resource_requests_memory_bytes) by (pod, namespace)
          - record: running_pod_memory_limit
            expr: kube_pod_container_resource_limits_memory_bytes * on (pod, namespace) group_left max(kube_pod_container_status_running) by (pod, namespace) > 0
          - record: running_pod_memory_request
            expr: kube_pod_container_resource_requests_memory_bytes * on (pod, namespace) group_left max(kube_pod_container_status_running) by (pod, namespace) > 0
          - record: running_pod_memory_usage
            expr: pod_memory_usage * on (pod, namespace) group_left max(kube_pod_container_status_running) by (pod, namespace) > 0

          - record: pod_cpu_request_usage
            expr: sum(pod_cpu_usage) by (namespace, pod) / pod_cpu_request
          - record: pod_cpu_limit_usage
            expr: sum(pod_cpu_usage) by (namespace, pod) / pod_cpu_limit

          - record: pod_memory_request_usage
            expr: sum(pod_memory_usage) by (namespace, pod) / pod_memory_request
          - record: pod_memory_limit_usage
            expr: sum(pod_memory_usage) by (namespace, pod) / pod_memory_limit

          - record: max_cpu_request_usage
            expr: max(max_over_time(pod_cpu_request_usage[14d])) by (pod, namespace)
          - record: max_memory_request_usage
            expr: max(max_over_time(pod_memory_request_usage[14d])) by (pod, namespace)

          - record: pod_restart
            expr: sum(idelta(kube_pod_container_status_restarts_total[5m]) > 0) by (pod,namespace)
          
  prometheus.yml: |-
    rule_files:
      - /etc/prometheus/alerts.yaml
      - /etc/prometheus/recorded-rules.yaml
      - /prometheus/alerts-additional/*.yaml

    global:
      scrape_interval: 15s
      evaluation_interval: 15s
      scrape_timeout: 5s

    alerting:
      alertmanagers:
      - path_prefix: /alertmanager
        kubernetes_sd_configs:
          - role: pod
        tls_config:
          ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
        bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/token
        relabel_configs:
        - source_labels: [__meta_kubernetes_pod_label_name]
          regex: alertmanager
          action: keep
        - source_labels: [__meta_kubernetes_namespace]
          regex: monitoring
          action: keep
        - source_labels: [__meta_kubernetes_pod_container_port_number]
          regex: ([0-9]+)
          action: keep

    scrape_configs:
      - job_name: 'prometheus'
        scrape_interval: 15s
        metrics_path: /prometheus/metrics/

        static_configs:
          - targets: ['localhost:9090']

      - job_name: 'kubernetes-scheduler'

        kubernetes_sd_configs:
        - role: pod

        relabel_configs:
        - source_labels: [__meta_kubernetes_pod_label_k8s_app]
          action: keep
          regex: kube-scheduler
        - source_labels: [__address__]
          action: replace
          regex: ([^:]+)
          replacement: $1:10251
          target_label: __address__
        - action: labelmap
          regex: __meta_kubernetes_pod_label_(.+)
        - source_labels: [__meta_kubernetes_namespace]
          action: replace
          target_label: kubernetes_namespace
        - source_labels: [__meta_kubernetes_pod_name]
          action: replace
          target_label: kubernetes_pod_name


      - job_name: 'kubernetes-controller-manager'

        kubernetes_sd_configs:
        - role: pod

        relabel_configs:
        - source_labels: [__meta_kubernetes_pod_label_k8s_app]
          action: keep
          regex: kube-controller-manager
        - source_labels: [__address__]
          action: replace
          regex: ([^:]+)
          replacement: $1:10252
          target_label: __address__
        - action: labelmap
          regex: __meta_kubernetes_pod_label_(.+)
        - source_labels: [__meta_kubernetes_namespace]
          action: replace
          target_label: kubernetes_namespace
        - source_labels: [__meta_kubernetes_pod_name]
          action: replace
          target_label: kubernetes_pod_name


      - job_name: 'kubernetes-apiservers'

        kubernetes_sd_configs:
        - role: endpoints

        # Default to scraping over https. If required, just disable this or change to
        # `http`.
        scheme: https

        # This TLS & bearer token file config is used to connect to the actual scrape
        # endpoints for cluster components. This is separate to discovery auth
        # configuration because discovery & scraping are two separate concerns in
        # Prometheus. The discovery auth config is automatic if Prometheus runs inside
        # the cluster. Otherwise, more config options have to be provided within the
        # <kubernetes_sd_config>.
        tls_config:
          ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
          # If your node certificates are self-signed or use a different CA to the
          # master CA, then disable certificate verification below. Note that
          # certificate verification is an integral part of a secure infrastructure
          # so this should only be disabled in a controlled environment. You can
          # disable certificate verification by uncommenting the line below.
          #
          # insecure_skip_verify: true
        bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/token

        # Keep only the default/kubernetes service endpoints for the https port. This
        # will add targets for each API server which Kubernetes adds an endpoint to
        # the default/kubernetes service.
        relabel_configs:
        - source_labels: [__meta_kubernetes_namespace, __meta_kubernetes_service_name, __meta_kubernetes_endpoint_port_name]
          action: keep
          regex: default;kubernetes;https

      # Scrape config for nodes (kubelet).
      #
      # Rather than connecting directly to the node, the scrape is proxied though the
      # Kubernetes apiserver.  This means it will work if Prometheus is running out of
      # cluster, or can't connect to nodes for some other reason (e.g. because of
      # firewalling).
      - job_name: 'kubernetes-nodes'

        # Default to scraping over https. If required, just disable this or change to
        # `http`.
        scheme: https

        # This TLS & bearer token file config is used to connect to the actual scrape
        # endpoints for cluster components. This is separate to discovery auth
        # configuration because discovery & scraping are two separate concerns in
        # Prometheus. The discovery auth config is automatic if Prometheus runs inside
        # the cluster. Otherwise, more config options have to be provided within the
        # <kubernetes_sd_config>.
        tls_config:
          ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
        bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/token

        kubernetes_sd_configs:
        - role: node

        relabel_configs:
        - action: labelmap
          regex: __meta_kubernetes_node_label_(.+)
        - target_label: __address__
          replacement: kubernetes.default.svc:443
        - source_labels: [__meta_kubernetes_node_name]
          regex: (.+)
          target_label: __metrics_path__
          replacement: /api/v1/nodes/${1}/proxy/metrics

      # The relabeling allows the actual service scrape endpoint to be configured
      # via the following annotations:
      #
      # * `prometheus.io/scrape`: Only scrape services that have a value of `true`
      # * `prometheus.io/scheme`: If the metrics endpoint is secured then you will need
      # to set this to `https` & most likely set the `tls_config` of the scrape config.
      # * `prometheus.io/path`: If the metrics path is not `/metrics` override this.
      # * `prometheus.io/port`: If the metrics are exposed on a different port to the
      # service then set this appropriately.
      - job_name: 'kubernetes-service-endpoints'

        kubernetes_sd_configs:
        - role: endpoints

        relabel_configs:
        - source_labels: [__meta_kubernetes_service_annotation_prometheus_io_scrape]
          action: keep
          regex: true
        - source_labels: [__meta_kubernetes_service_annotation_prometheus_io_scheme]
          action: replace
          target_label: __scheme__
          regex: (https?)
        - source_labels: [__meta_kubernetes_service_annotation_prometheus_io_path]
          action: replace
          target_label: __metrics_path__
          regex: (.+)
        - source_labels: [__address__, __meta_kubernetes_service_annotation_prometheus_io_port]
          action: replace
          target_label: __address__
          regex: ([^:]+)(?::\d+)?;(\d+)
          replacement: $1:$2
        - action: labelmap
          regex: __meta_kubernetes_service_label_(.+)
        - source_labels: [__meta_kubernetes_namespace]
          action: replace
          target_label: kubernetes_namespace
        - source_labels: [__meta_kubernetes_service_name]
          action: replace
          target_label: kubernetes_name

      - job_name: 'kubernetes-cadvisor'
        # Default to scraping over https. If required, just disable this or change to
        # `http`.
        scheme: https

        # This TLS & bearer token file config is used to connect to the actual scrape
        # endpoints for cluster components. This is separate to discovery auth
        # configuration because discovery & scraping are two separate concerns in
        # Prometheus. The discovery auth config is automatic if Prometheus runs inside
        # the cluster. Otherwise, more config options have to be provided within the
        # <kubernetes_sd_config>.
        tls_config:
          ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
        bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/token

        kubernetes_sd_configs:
        - role: node

        relabel_configs:
        - action: labelmap
          regex: __meta_kubernetes_node_label_(.+)
        - target_label: __address__
          replacement: kubernetes.default.svc:443
        - source_labels: [__meta_kubernetes_node_name]
          regex: (.+)
          target_label: __metrics_path__
          replacement: /api/v1/nodes/${1}/proxy/metrics/cadvisor

      # The relabeling allows the actual pod scrape endpoint to be configured via the
      # following annotations:
      #
      # * `prometheus.io/scrape`: Only scrape pods that have a value of `true`
      # * `prometheus.io/path`: If the metrics path is not `/metrics` override this.
      # * `prometheus.io/port`: Scrape the pod on the indicated port instead of the
      # pod's declared ports (default is a port-free target if none are declared).
      - job_name: 'kubernetes-pods'

        kubernetes_sd_configs:
        - role: pod

        relabel_configs:
        - source_labels: [__meta_kubernetes_pod_annotation_prometheus_io_scrape]
          action: keep
          regex: true
        - source_labels: [__meta_kubernetes_pod_annotation_prometheus_io_path]
          action: replace
          target_label: __metrics_path__
          regex: (.+)
        - source_labels: [__address__, __meta_kubernetes_pod_annotation_prometheus_io_port]
          action: replace
          regex: ([^:]+)(?::\d+)?;(\d+)
          replacement: $1:$2
          target_label: __address__
        - action: labelmap
          regex: __meta_kubernetes_pod_label_(.+)
        - source_labels: [__meta_kubernetes_namespace]
          action: replace
          target_label: kubernetes_namespace
        - source_labels: [__meta_kubernetes_pod_name]
          action: replace
          target_label: kubernetes_pod_name
        - source_labels: [__meta_kubernetes_pod_node_name]
          action: replace
          target_label: host

